# libDSAL

[![pipeline status](https://shields.io/gitlab/pipeline/TheDreche/devent/main?logo=gitlab)](https://gitlab.com/TheDreche/dsal/-/pipelines) [![lines of code](https://shields.io/tokei/lines/gitlab/TheDreche/dsal)](https://gitlab.com/TheDreche/dsal/) [![code coverage](https://img.shields.io/gitlab/coverage/TheDreche/dsal/main)](https://gitlab.com/TheDreche/dsal/-/pipelines)

A C++ library for abstracting operating system functions.

This can act as a C++ version of the standard C library. However, this actually uses the standard C library and some operating system dependent functions if needed. It is designed to make it easy to add another operating system.

## Installation

The dependencies are not very many:
- A C++ compiler, supporting C++11
- The following standard library headers (along the actual standard library implementation):
	+ algorithm
	+ cerrno
	+ cstddef
	+ cstdint
	+ cstdio
	+ cstdlib
	+ cstring
	+ limits
	+ new
	+ typeinfo
	+ utility

After cloning this repository *with submodules(!)*, run
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~sh
autoreconf --verbose --install
./configure
make
sudo make install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The configure script accepts some options not mentioned in [INSTALL](./INSTALL):
- `--enable-doc`: Enable building documentation using doxygen
	+ `--enable-doc-html`: Build HTML documentation
	+ `--enable-doc-man`: Build man pages
	+ `--with-doc-dot`: Use dot for generating documentation. May take the path to dot as in `--with-doc-dot=/path/to/dot`

For more information, see [INSTALL](./INSTALL).

In case you plan developing, you may find the following options useful:
- `--enable-debug`: Compile code for debugging support
- `--enable-doc-internal`: Generate internal documentation (including reference list, ...)
- `--enable-doc-warn`: Write warning messages to `doc/Doxywarn`

## Origin

I began to saw the problems with the C++ standard library. At that time, I haven't seen the ones of the C one, and thus I tried somehow porting it. However, I quickly realized that the C standard library also had its problems (like undefined encoding of strings) so I began on making my completely custom standard library built on top of the actual C++ standard library.

Some things that don't fit my personal taste in the C++ standard library will be implemented completely different.

Some examples on what I plan to make different:
- Lists should not be used very often. Iterators fulfil the job.
- Don't have a separate string class; use iterators again. This, for example, even enables reading the contents of a file without having all of them in memory!
- metaprogramming should get as easy as usual programming (except for the long compiler error messages; they probably can't be fixed by a library; if you know a way, create an issue about it!)

## Project structure

`libDSAL` will get split into the following parts (sorted by alphabet):

- concepts
- connect (connect(3), sockets, ...)
- data (basic data structures)
- device
- error (utilities and very general errors)
- filesystem
- itermod
- library (load dynamic library, ...)
- locale
- math
- memory
- meta (meta programming, everything is evaluated at compile time)
- process
- regex
- time
- user
- vararg

These parts have mostly been selected by looking through the manual page [`system_data_types(7)`](https://man.archlinux.org/man/core/man-pages/system_data_types.7.en).

## Parts left to do

I will focus on the following parts as I would need them the most:

- itermod
- filesystem

## Packaging

I probably won't package this by myself for now.

However, if you want to package this, feel free: This is just a basic autotools setup and independent of the system. So, the usual packaging template of running `autoreconf --install && ./configure --prefix=/usr && make && make DESTDIR=... install` will work (if this repository is cloned with submodules).
