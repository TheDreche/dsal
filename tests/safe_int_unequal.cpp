/* Copyright (C) 2021 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dsal.hpp"

#include <cstdio>
#include <cstdint>

bool safe_int_unequal_normal() {
	std::printf("Trying to check for unequal safe_ints ...\n");
	try {
		std::uint8_t i1 = 2;
		dsal::math::safe_int<std::uint8_t> i2 = 3;
		if(i1 != i2) {
			return false;
		}
	} catch(...) {}
	return true;
}

bool safe_int_unequal_overflow() {
	std::printf("Trying to check for overflow unequal safe_ints ...\n");
	try {
		dsal::math::safe_int<std::int8_t> i1 = dsal::math::safe_int<std::int8_t>::max;
		std::int16_t i2 = INT16_MAX;
		if(i1 != i2) {
			return false;
		}
	} catch(...) {}
	return true;
}

bool safe_int_unequal_underflow() {
	std::printf("Trying to check for underflow unequal safe_ints ...\n");
	try {
		dsal::math::safe_int<std::int8_t> i1 = dsal::math::safe_int<std::int8_t>::min;
		std::int16_t i2 = INT16_MIN;
		if(i1 != i2) {
			return false;
		}
	} catch(...) {}
	return true;
}

int main() {
	if(safe_int_unequal_normal()) {
		goto failure;
	}
	if(safe_int_unequal_overflow()) {
		goto failure;
	}
	if(safe_int_unequal_underflow()) {
		goto failure;
	}
	
	return 0;
failure:
	return 1;
}
