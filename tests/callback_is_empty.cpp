/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dsal.hpp"

#include <cstdio>

static int i = 0;

int set(int val) {
	i = val + 1;
	return val + 2;
}

bool nonempty_is_empty() {
	std::printf("Checking whether nonempty callback is empty ...\n");
	bool result = false;
	
	dsal::data::callback<int(int)> cb_full = set;
	
	if(cb_full.is_empty()) {
		std::printf("Callback that has a value returns being empty.\n");
		result = true;
	}
	
	return result;
}

bool empty_is_empty() {
	std::printf("Checking whether empty callback is empty ...\n");
	bool result = false;
	
	dsal::data::callback<int(int)> cb_empty;
	
	if(!cb_empty.is_empty()) {
		std::printf("Callback that has no value returns having one.\n");
		result = true;
	}
	return result;
}

int main() {
	if(empty_is_empty()) {
		goto failure;
	}
	if(nonempty_is_empty()) {
		goto failure;
	}
	
success:
	return 0;
failure:
	return 1;
}
