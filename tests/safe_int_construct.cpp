/* Copyright (C) 2021 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dsal.hpp"

#include <cstdio>
#include <cstdint>

bool safe_int_construct() {
	std::printf("Constructint safe_int<uint8_t> ...\n");
	dsal::math::safe_int<std::uint8_t> i = 5;
	if((std::uint8_t) i != 5) {
		return true;
	}
	return false;
}

int main() {
	if(safe_int_construct()) {
		goto failure;
	}
	
	return 0;
failure:
	return 1;
}
