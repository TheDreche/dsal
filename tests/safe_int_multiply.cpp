/* Copyright (C) 2021 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dsal.hpp"

#include <cstdio>
#include <cstdint>

bool safe_int_multiply_normal() {
	std::printf("Trying to multiply safe_ints without error ...\n");
	try {
		dsal::math::safe_int<std::uint8_t> i1 = 5;
		dsal::math::safe_int<std::uint8_t> i2 = 3;
		if(i1 * i2 != 15) {
			return true;
		} else {
			return false;
		}
	} catch(...) {
		return true; // failure
	}
	return false;
}

bool safe_int_multiply_zero() {
	std::printf("Trying to multiply safe_int by zero ...\n");
	try {
		dsal::math::safe_int<std::uint8_t> i1 = 5;
		dsal::math::safe_int<std::uint8_t> i2 = 0;
		if(i1 * i2 != 0) {
			return true;
		} else {
			return false;
		}
	} catch(...) {
		return true; // failure
	}
	return false;
}

bool safe_int_multiply_overflow() {
	std::printf("Trying to multiply safe_ints with overflow ...\n");
	try {
		dsal::math::safe_int<std::int8_t> i1 = dsal::math::safe_int<std::int8_t>::max;
		dsal::math::safe_int<std::int8_t> i2 = 3;
		if(i1 * i2 != 5) {
			printf("Didn't overflow???\n");
			return true;
		} else {
			return true; // failure
		}
	} catch(dsal::math::error_overflow) {
		return false; // success
	} catch(...) {
		return true;
	}
}

bool safe_int_multiply_underflow() {
	std::printf("Trying to multiply safe_int with underflow ...\n");
	try {
		dsal::math::safe_int<std::int8_t> i1 = dsal::math::safe_int<std::int8_t>::min;
		dsal::math::safe_int<std::int8_t> i2 = 3;
		if(i1 * i2 != 5) {
			printf("Didn't underflow???\n");
			return true;
		} else {
			return true; // failure
		}
	} catch(dsal::math::error_underflow) {
		return false; // success
	} catch(...) {
		return true;
	}
}

bool safe_int_multiply_overflow_min_minusone() {
	std::printf("Trying to multiply safe_ints min and -1\n");
	try {
		dsal::math::safe_int<std::int8_t> i1 = dsal::math::safe_int<std::int8_t>::min;
		dsal::math::safe_int<std::int8_t> i2 = -1;
		if(i1 * i2 != 5) {
			printf("Didn't overflow???\n");
			return true;
		} else {
			return true; // failure
		}
	} catch(dsal::math::error_overflow) {
		return false; // success
	} catch(...) {
		return true;
	}
}

bool safe_int_multiply_overflow_minusone_min() {
	std::printf("Trying to multiply safe_ints min and -1\n");
	try {
		dsal::math::safe_int<std::int8_t> i1 = -1;
		dsal::math::safe_int<std::int8_t> i2 = dsal::math::safe_int<std::int8_t>::min;
		if(i1 * i2 != 5) {
			printf("Didn't overflow???\n");
			return true;
		} else {
			return true; // failure
		}
	} catch(dsal::math::error_overflow) {
		return false; // success
	} catch(...) {
		return true;
	}
}

int main() {
	if(safe_int_multiply_normal()) {
		goto failure;
	}
	if(safe_int_multiply_zero()) {
		goto failure;
	}
	if(safe_int_multiply_overflow()) {
		goto failure;
	}
	if(safe_int_multiply_underflow()) {
		goto failure;
	}
	if(safe_int_multiply_overflow_min_minusone()) {
		goto failure;
	}
	if(safe_int_multiply_overflow_minusone_min()) {
		goto failure;
	}
	
	return 0;
failure:
	return 1;
}
