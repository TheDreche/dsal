/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dsal.hpp"

#include <cstdio>

bool failed = false;
std::size_t constructions = 0;
std::size_t destructions = 0;

class testeritem final {
	public:
		testeritem() {
			std::printf("Construct\n");
			++constructions;
		}
		testeritem(const testeritem&) {
			std::printf("Construct: Copy\n");
			++constructions;
		}
		testeritem(testeritem&&) {
			std::printf("Construct: Move\n");
			++constructions;
		}
		testeritem& operator=(const testeritem&) {
			std::printf("Assign: Copy\n");
			return *this;
		}
		testeritem& operator=(testeritem&&) {
			std::printf("Assign: Move\n");
			return *this;
		}
		~testeritem() {
			std::printf("Destruct\n");
			++destructions;
		}
};

// Instanciate templates to be tested
template class dsal::data::array<testeritem, 5>;
template class dsal::data::array<testeritem, 3>;
template class dsal::data::array<int, 7>;
template class dsal::concepts::iterator<testeritem>;
template class dsal::concepts::forward_iterator<testeritem>;
template class dsal::concepts::bidirectional_iterator<testeritem>;
template class dsal::concepts::random_access_iterator<testeritem>;
template class dsal::concepts::iterator<int>;
template class dsal::concepts::forward_iterator<int>;
template class dsal::concepts::bidirectional_iterator<int>;
template class dsal::concepts::random_access_iterator<int>;

#define assert(condition) if(!(condition)) { std::printf("! Assertion failed: %s (%s, %i) !\n", #condition, __FILE__, __LINE__); failed = true; return; }

void check1() {
	std::printf(">>> Checking construction and destruction ...\n");
	{
		dsal::data::array<testeritem, 5> data;
	}
	assert(constructions == destructions);
}

void check2() {
	std::printf(">>> Checking copy construction ...\n");
	{
		dsal::data::array<testeritem, 5> data;
		dsal::data::array<testeritem, 5> data_cpy = data;
	}
	assert(constructions == destructions);
}

void check3() {
	std::printf(">>> Checking copy assignment ...\n");
	{
		dsal::data::array<testeritem, 5> data;
		dsal::data::array<testeritem, 5> data2;
		data = data2;
	}
	assert(constructions == destructions);
}

void check4() {
	std::printf(">>> Checking size validity ...\n");
	std::size_t gotten_size;
	{
		dsal::data::array<testeritem, 3> data;
		gotten_size = data.size();
	}
	assert(gotten_size == 3);
}

class counting_iterator final : public dsal::concepts::iterator<int> {
	private:
		mutable int at = 0;
	public:
		void next() final override {
			++at;
		}
		int* pointer() const final override {
			return &at;
		}
		counting_iterator& operator++() final override {
			next();
			return *this;
		}
};

void check5() {
	std::printf(">>> Initialize from counting iterator and check ...\n");
	{
		counting_iterator c;
		dsal::data::array<int, 7> data = c;
		int expected = 0;
		for(int& i : data) {
			assert(i == expected);
			++expected;
		}
	}
}

void check6() {
	std::printf(">>> Set array manually to counting ...\n");
	std::printf("? Checks iterator++ instead of ++iterator\n");
	{
		dsal::data::array<int, 7> data;
		dsal::data::array<int, 7>::iterator iter = data.begin();
		for(int at = 0; at < 7; ++at) {
			*iter++ = at;
		}
		int expected = 0;
		for(int& i : data) {
			assert(i == expected);
			++expected;
		}
	}
}

void check7() {
	std::printf(">>> Count with exception: [2] = 7\n");
	std::printf("? Checks array::operator[]: No *nullptr\n");
	{
		counting_iterator c;
		dsal::data::array<int, 7> data = c;
		data[2] = 7;
	}
}

int main() {
	check1();
	check2();
	check3();
	check4();
	check5();
	check6();
	check7();
	return failed? 1 : 0;
}
