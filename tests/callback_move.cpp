/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dsal.hpp"

#include <cstdio>

#include <algorithm> // std::move

static int i = 0;

int set(int val) {
	i = val + 1;
	return val + 2;
}

bool move_function() {
	std::printf("Moving function ...\n");
	bool result = false;
	
	dsal::data::callback<int(int)> cb_ = set;
	dsal::data::callback<int(int)> cb = std::move(cb_);
	int r = cb(2);
	
	if(i != 3) {
		std::printf("Variable has wrong value! Expected: %i, has: %i\n", 3, i);
		result = true;
	}
	if(r != 4) {
		std::printf("Function has returned wrong value! Expected: %i, returned: %i\n", 4, r);
		result = true;
	}
	
	return result;
}

bool move_lambda() {
	std::printf("Moving lambda ...\n");
	bool result = false;
	int i = 0;
	
	dsal::data::callback<int(int)> cb_ = [&i](int val){i = val + 1; return val + 2;};
	dsal::data::callback<int(int)> cb = std::move(cb_);
	int r = cb(2);
	
	if(i != 3) {
		std::printf("Variable has wrong value! Expected: %i, has: %i\n", 3, i);
		result = true;
	}
	if(r != 4) {
		std::printf("Function has returned wrong value! Expected: %i, returned: %i\n", 4, r);
		result = true;
	}
	
	return result;
}

int main() {
	if(move_function()) {
		goto failure;
	}
	if(move_lambda()) {
		goto failure;
	}
	
success:
	return 0;
failure:
	return 1;
}
