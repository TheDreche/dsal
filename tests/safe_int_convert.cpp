/* Copyright (C) 2021 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dsal.hpp"

#include <cstdio>
#include <cstdint>

bool safe_int_convert_normal() {
	std::printf("Trying to convert safe_int without error ...\n");
	try {
		dsal::math::safe_int<std::uint16_t> i = dsal::math::safe_int<std::uint8_t>(dsal::math::safe_int<std::uint8_t>::max);
	} catch(...) {
		return true; // failure
	}
	return false;
}

bool safe_int_convert_overflow() {
	std::printf("Trying to convert safe_int with overflow ...\n");
	try {
		dsal::math::safe_int<std::uint8_t> i = dsal::math::safe_int<std::uint16_t>(dsal::math::safe_int<std::uint16_t>::max);
	} catch(dsal::math::error_overflow) {
		return false;
	} catch(...) {
		return true; // failure
	}
	return true;
}

bool safe_int_convert_underflow() {
	std::printf("Trying to convert safe_int with underflow ...\n");
	try {
		dsal::math::safe_int<std::int8_t> i = dsal::math::safe_int<std::int16_t>(dsal::math::safe_int<std::int16_t>::min);
	} catch(dsal::math::error_underflow) {
		return false;
	} catch(...) {
		return true; // failure
	}
	return true;
}

int main() {
	if(safe_int_convert_normal()) {
		goto failure;
	}
	if(safe_int_convert_overflow()) {
		goto failure;
	}
	if(safe_int_convert_underflow()) {
		goto failure;
	}
	
	return 0;
failure:
	return 1;
}
