/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_HPP
#define DSAL_HPP

/*!
 * @file
 * @brief A file for quickly including everything libdsal related.
 */

/*!
 * @brief Namespace containing the whole libdsal library.
 */
namespace dsal {}

/* For vim/gvim users:
 * 
 * You can update this file easily using
 * 
 * 	:read !find include/dsal/ -maxdepth 1 -name '*.hpp' | sort | sed 's+include/+\#include "+;s+$+"+'
 */

#include "dsal/concepts.hpp"
#include "dsal/data.hpp"
#include "dsal/error.hpp"
#include "dsal/math.hpp"
#include "dsal/memory.hpp"

#endif
