/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_MEMORY__INTERNAL_STDALLOCERR_HPP
#define DSAL_MEMORY__INTERNAL_STDALLOCERR_HPP

/*!
 * @internal
 * @file
 * @brief Defines the function checking for malloc, calloc, ... errors.
 * @sa dsal::memory::_internal::stdalloc_chk_err
 */

#include <cerrno>

#include "../error.hpp"
#include "../error_memory_not_enough.hpp"

#include "../size.hpp"

namespace dsal {
	namespace memory {
		namespace _internal {
			template<class allocated_type>
			allocated_type* stdalloc_chk_err(void* ret) {
				if(ret == nullptr) {
					if(sizeof(allocated_type) > 0) {
						switch(errno) {
							case ENOMEM:
								throw dsal::memory::error_memory_not_enough();
							default:
								throw dsal::memory::error();
						}
					}
				}
				return (allocated_type*) ret;
			}
		}
	}
}

#endif
