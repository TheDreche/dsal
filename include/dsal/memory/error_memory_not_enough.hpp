/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_MEMORY_ERROR_MEMORY_NOT_ENOUGH_HPP
#define DSAL_MEMORY_ERROR_MEMORY_NOT_ENOUGH_HPP

/*!
 * @file
 * @brief The file defining the @ref dsal::memory::error_memory_not_enough error.
 */

#include "error.hpp"

namespace dsal {
	namespace memory {
		/*!
		 * @brief Not enough free memory is availible.
		 * 
		 * This should be thrown when the system ran out of free memory and needs more to do the operation.
		 */
		class error_memory_not_enough : public error {
			public:
				virtual const char* what() const noexcept override;
				virtual const char* what_localized() const override;
				virtual const char* what_dev() const noexcept override;
				virtual const char* what_code() const noexcept override;
				virtual const char* what_code_localized() const override;
				virtual const char* what_code_dev() const noexcept override;
		};
	}
}

#endif
