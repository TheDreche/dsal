/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_MEMORY_DESTRUCT_HPP
#define DSAL_MEMORY_DESTRUCT_HPP

/*!
 * @file
 * @brief The file defining the dsal::memory::destruct function.
 */

namespace dsal {
	namespace memory {
		/*!
		 * @brief Destruct an object at a given position.
		 * 
		 * @tparam type The type of the object to destruct.
		 * @param location The place the object to be destructed resides.
		 * @return nullptr.
		 */
		template<class type>
		type* destruct(type* location) {
			location->~type();
			return nullptr;
		}
	}
}

#endif
