/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_MEMORY_CONSTRUCT_HPP
#define DSAL_MEMORY_CONSTRUCT_HPP

/*!
 * @file
 * @brief The file defining the dsal::memory::construct function.
 */

#include <new>
#include <utility>

namespace dsal {
	namespace memory {
		/*!
		 * @brief Construct an object at a given position.
		 * 
		 * @tparam type The type of the object to construct.
		 * @tparam Args The constructor argument types.
		 * @param location The place where to construct the object.
		 * @param args The constructor arguments.
		 * @return A reference to the newly constructed object.
		 */
		template<class type, class... Args>
		type& construct(type* location, const Args&... args) {
			new(location) type(std::forward<Args>(args)...);
			return *location;
		}
	}
}

#endif
