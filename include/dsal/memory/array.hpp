/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_MEMORY_ARRAY_HPP
#define DSAL_MEMORY_ARRAY_HPP

/*!
 * @file
 * @brief A file declaring the dsal::memory::array class.
 */

#include <cstdlib>

#include "../math/safe_int.hpp"
#include "_internal/stdallocerr.hpp"

#include "size.hpp"

namespace dsal {
	namespace memory {
		/*!
		 * @brief An array memory resource.
		 * 
		 * This represents a dynamically allocated array memory resource.
		 * 
		 * This class doesn't take care about initializing its contents or destructing them. You can't copy objects of this class.
		 * 
		 * @tparam _T The item type of the array.
		 */
		template<class _T>
		class array final {
			public:
				/*!
				 * @brief The item type.
				 */
				typedef _T item;
				
				/*!
				 * @brief The item type size.
				 * 
				 * Every item needs this many bytes of memory.
				 */
				static const size item_size = sizeof(item);
				
			private:
				item* storage = nullptr;
				
			public:
				/*!
				 * @brief Create an array having a specific length.
				 * 
				 * No element will be initialized.
				 * 
				 * @param new_size The length of the array.
				 */
				array(size new_size = 0) : storage(dsal::memory::_internal::stdalloc_chk_err<item>(std::calloc(new_size, item_size))) {}
				array(const array<item>&) = delete;
				/*!
				 * @brief Move another array.
				 * 
				 * The other array won't be usable anymore.
				 * 
				 * @param o The other array.
				 */
				array(array<item>&& o) : storage(o.storage) {
					o.storage = nullptr;
				}
				~array() {
					std::free(storage);
					storage = nullptr;
				}
				
				array<item>& operator=(const array<item>&) = delete;
				/*!
				 * @brief Move another array into this one.
				 * 
				 * The other array won't be usable afterwards.
				 * 
				 * @param o The other array.
				 */
				array<item>& operator=(array<item>&& o) {
					std::free(storage);
					storage = o.storage;
					o.storage = nullptr;
					return *this;
				}
				
				/*!
				 * @brief The item at a given position.
				 * 
				 * @param position The position in the array of the item to access.
				 * @return A reference to the specified item.
				 */
				item& at(size position) {
					return storage[position];
				}
				/*!
				 * @copydoc at(size)
				 */
				const item& at(size position) const {
					return storage[position];
				}
				/*!
				 * @copydoc at(size)
				 */
				item& operator[](size position) {
					return at(position);
				}
				/*!
				 * @copydoc at(size) const
				 */
				const item& operator[](size position) const {
					return at(position);
				}
				
				/*!
				 * @brief Pointer to this array.
				 * 
				 * The pointer points to the beginning of the array. Increasing it by 1 means moving on to the next element.
				 * 
				 * @return A pointer to the first item in this array.
				 */
				item* pointer() {
					return storage;
				}
				/*!
				 * @copydoc pointer()
				 */
				const item* pointer() const {
					return storage;
				}
				/*!
				 * @copydoc pointer()
				 */
				operator item*() {
					return pointer();
				}
				/*!
				 * @copydoc pointer() const
				 */
				operator const item*() const {
					return pointer();
				}
		};
	}
}

#endif
