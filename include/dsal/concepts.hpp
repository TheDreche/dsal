/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_CONCEPTS_HPP
#define DSAL_CONCEPTS_HPP

/*!
 * @file
 * @brief A file for quickly including all concepts.
 * 
 * A concept is, more or less, a specification for classes.
 */

namespace dsal {
	/*!
	 * @brief Collection of concepts.
	 * 
	 * A concept is like a specification for a class. Thus, they are usually represented by abstract classes.
	 */
	namespace concepts {}
}

/* For vim/gvim users:
 * 
 * You can update this file easily using
 * 
 * 	:read !find include/dsal/concepts/ -maxdepth 1 -name '*.hpp' | sort | sed 's+include/dsal/+\#include "+;s+$+"+'
 */

#include "concepts/bidirectional_iterator.hpp"
#include "concepts/forward_iterator.hpp"
#include "concepts/iterator.hpp"
#include "concepts/random_access_iterator.hpp"

#endif
