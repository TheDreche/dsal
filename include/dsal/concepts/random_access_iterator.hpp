/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_CONCEPTS_RANDOM_ACCESS_ITERATOR_HPP
#define DSAL_CONCEPTS_RANDOM_ACCESS_ITERATOR_HPP

/*!
 * @file
 * @brief The file defining the @ref dsal::concepts::random_access_iterator abstract class.
 */

#include "../memory/size_difference.hpp"
#include "bidirectional_iterator.hpp"

namespace dsal {
	namespace concepts {
		/*!
		 * @brief A random access iterator.
		 * 
		 * Random access iterators have the property that you can specify any offset of the current element and you can get the element at that offset, usually in O(1).
		 * 
		 * It is assumed every random access iterator is also a bidirectional iterator.
		 * 
		 * The actual classes also should implement comparisons and addition/subtraction (operator+ and operator- with dsal::memory::size_difference as argument).
		 * 
		 * @tparam Type The type being iterated over.
		 */
		template<class Type>
		class random_access_iterator : public bidirectional_iterator<Type> {
			public:
				/*!
				 * @brief Change this iterator by the specified offset.
				 * 
				 * If the offset is positive, it will move forward, if it is negative, it will move backwards, and if it is 0, it should stay where it is.
				 * 
				 * @param offset The offset to change this iterator by.
				 */
				virtual void change(dsal::memory::size_difference offset) = 0;
				
				/*!
				 * @brief Move forward by the specified offset.
				 * 
				 * If the offset is negative, move backward.
				 * 
				 * @param offset The offset to move forward.
				 */
				void move_forward(dsal::memory::size_difference offset) {
					change(offset);
				}
				
				/*!
				 * @brief Move backward by the specified offset.
				 * 
				 * If the offset is negative, move forward.
				 * 
				 * @param offset The offset to move backward by.
				 */
				void move_back(dsal::memory::size_difference offset) {
					change(0 - offset);
				}
				
				/*!
				 * @copydoc move_forward(dsal::memory::size_difference)
				 * @return A reference to this iterator.
				 */
				virtual random_access_iterator<Type>& operator+=(dsal::memory::size_difference offset) = 0;
				
				/*!
				 * @copydoc move_back(dsal::memory::size_difference)
				 * @return A reference to this iterator.
				 */
				virtual random_access_iterator<Type>& operator-=(dsal::memory::size_difference offset) = 0;
				
				// Override iterator / bidirectional_iterator functions
				/*!
				 * @copydoc dsal::concepts::iterator<item>::next()
				 */
				void next() final override {
					move_forward(1);
				}
				/*!
				 * @copydoc dsal::concepts::bidirectional_iterator<item>::previous()
				 */
				void previous() final override {
					move_back(1);
				}
				/*!
				 * @copydoc dsal::concepts::bidirectional_iterator<item>::operator--()
				 */
				virtual random_access_iterator<Type>& operator--() override = 0;
				/*!
				 * @copydoc dsal::concepts::iterator<item>::operator++()
				 */
				virtual random_access_iterator<Type>& operator++() override = 0;
		};
	}
}

#endif
