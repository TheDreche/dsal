/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_CONCEPTS_FORWARD_ITERATOR_HPP
#define DSAL_CONCEPTS_FORWARD_ITERATOR_HPP

/*!
 * @file
 * @brief The file defining the @ref dsal::concepts::forward_iterator abstract class.
 */

#include "iterator.hpp"

namespace dsal {
	namespace concepts {
		/*!
		 * @brief A forward iterator.
		 * 
		 * A forward iterator is an iterator where:
		 * - Copying is always possible
		 * - Increasing a copy doesn't change the original iterator or whether it is valid
		 * 
		 * Theoretically all of that can be implemented in a usual @ref iterator, but having this type makes it more clear whether the behaviour is defined.
		 * 
		 * @tparam Type The item type of the container being iterated over.
		 */
		template<class Type>
		class forward_iterator : public iterator<Type> {};
	}
}

#endif
