/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_CONCEPTS_ITERATOR_HPP
#define DSAL_CONCEPTS_ITERATOR_HPP

/*!
 * @file
 * @brief The file defining the @ref dsal::concepts::iterator abstract class.
 */

namespace dsal {
	namespace concepts {
		/*!
		 * @brief An iterator.
		 * 
		 * Every iterator type should inherit from this.
		 * 
		 * An iterator is an object used for going through lists, one element after another.
		 * 
		 * Iterators originally were pointers, so dereferencing an iterator means gaining a reference to the current element.
		 * 
		 * @tparam Type The item type of the container being iterated over.
		 */
		template<class Type>
		class iterator {
			public:
				/*!
				 * @brief The item type of the container being iterated over.
				 */
				typedef Type item;
			public:
				virtual ~iterator() {}
				
				/*!
				 * @brief Move on to the next item.
				 */
				virtual void next() = 0;
				
				/*!
				 * @brief Pointer to the current element.
				 * 
				 * @return A pointer to the current element.
				 */
				virtual item* pointer() const = 0;
				
				/*!
				 * @copydoc next()
				 * @return A reference to this iterator.
				 */
				virtual iterator<item>& operator++() = 0;
				
				/*!
				 * @copydoc pointer()
				 */
				operator item*() const {
					return pointer();
				}
				
				/*!
				 * @brief Access a member of the current item.
				 * 
				 * @return A pointer to the current item.
				 */
				item* operator->() const {
					return pointer();
				}
				
				/*!
				 * @brief Dereference this iterator.
				 * 
				 * Get the current item. If this iterator is before the first item or after the last one and is tried to dereference, undefined behaviour has occurred.
				 * 
				 * @return A reference to the current item.
				 */
				item& dereference() const {
					return *pointer();
				}
				
				/*!
				 * @copydoc dereference()
				 */
				item& operator*() const {
					return dereference();
				}
		};
	}
}

#endif
