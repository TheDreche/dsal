/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_CONCEPTS_BIDIRECTIONAL_ITERATOR_HPP
#define DSAL_CONCEPTS_BIDIRECTIONAL_ITERATOR_HPP

/*!
 * @file
 * @brief The file defining the @ref dsal::concepts::bidirectional_iterator abstract class.
 */

#include "forward_iterator.hpp"

namespace dsal {
	namespace concepts {
		/*!
		 * @brief An iterator movable in both directions.
		 * 
		 * A bidirectional iterator can be moved in both directions: If you have moved one item further than you wanted to, you can decrease the bidirectional iterator to go back.
		 * 
		 * It is assumed that every bidirectional iterator is also a @ref forward_iterator.
		 * 
		 * @tparam Type The type being iterated over.
		 */
		template<class Type>
		class bidirectional_iterator : public forward_iterator<Type> {
			public:
				/*!
				 * @brief Go back to the previous element.
				 */
				virtual void previous() = 0;
				
				/*!
				 * @copydoc previous()
				 * 
				 * @return A reference to this iterator.
				 */
				virtual bidirectional_iterator<Type>& operator--() = 0;
		};
	}
}

#endif
