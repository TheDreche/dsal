/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_MATH_ERROR_UNDERFLOW_TYPED_HPP
#define DSAL_MATH_ERROR_UNDERFLOW_TYPED_HPP

/*!
 * @file
 * @brief The file defining the dsal::math::error_underflow_typed error.
 */

#include "error_underflow.hpp"

namespace dsal {
	namespace math {
		/*!
		 * @brief An underflow would have happened.
		 * 
		 * This should be thrown if an action was requested that would result in an underflow.
		 * 
		 * An underflow is if the number is too low to fit into its type.
		 * 
		 * @tparam _int_t The integer type being too small.
		 */
		template<class _int_t>
			class error_underflow_typed : public error_underflow {
				public:
					/*!
					 * @brief The integer type that was too small.
					 */
					typedef _int_t integer_type;
					
					virtual const char* what() const noexcept override {
						return "A numer too large has been calculated.\n";
					}
					virtual const char* what_localized() const override {
						return nullptr;
					}
					virtual const char* what_dev() const noexcept override {
						return
							"error type: dsal::math::error_underflow_typed\n"
							"<No details availible>\n";
					}
					virtual const char* what_code() const noexcept override {
						return "underflow";
					}
					virtual const char* what_code_localized() const override {
						return nullptr;
					}
					virtual const char* what_code_dev() const noexcept override {
						return "dsal::math::error_underflow_typed";
					}
			};
	}
}

#endif
