/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_MATH_SAFE_INT_HPP
#define DSAL_MATH_SAFE_INT_HPP

/*!
 * @file
 * @brief The file defining the dsal::math::safe_int template class.
 */

#include <limits>

#include "error_division_by_zero_typed.hpp"
#include "error_overflow_typed.hpp"
#include "error_underflow_typed.hpp"

namespace dsal {
	namespace math {
		/*!
		 * @brief A safe integer wrapper.
		 * 
		 * When using this wrapper, you don't need to care about overflows anymore as much.
		 * 
		 * If an overflow happens, a dsal::math::error_overflow_typed will be thrown.
		 * If an underflow happens, a dsal::math::error_underflow_typed will be thrown.
		 * If you divide by zero, a dsal::math::error_division_by_zero_typed will be thrown.
		 * 
		 * Make sure _int_type is an integer type such as unsigned long int and not any other number type such as float, as that may result in undefined behaviour.
		 * 
		 * @tparam _int_type The type of the integer.
		 */
		template<class _int_type>
			class safe_int {
				public:
					/*!
					 * @brief The integer type this class wraps.
					 */
					typedef _int_type integer_type;
					
					/*!
					 * @brief The maximum value this class can save.
					 */
					const static integer_type max = std::numeric_limits<integer_type>::max();
					
					/*!
					 * @brief The minimum value this class can save.
					 */
					const static integer_type min = std::numeric_limits<integer_type>::lowest();
					
				private:
					/*!
					 * @brief The actual value.
					 */
					integer_type value;
					
				public:
					safe_int() {}
					
					/*!
					 * @brief Convert the true type to this.
					 */
					safe_int(integer_type i) : value(i) {}
					
					/*!
					 * @brief Convert this to the real integer type.
					 * 
					 * @return The integer this wraps.
					 */
					operator integer_type() const {
						return value;
					}
					
					/*!
					 * @brief Convert this to another integer type.
					 * 
					 * @tparam _new_integer_type The new integer type.
					 * @return The result of the conversion to the new integer_type type.
					 * @throw dsal::math::error_overflow_typed<_new_integer_type> If the current value is too big to fit into the new type.
					 * @throw dsal::math::error_underflow_typed<_new_integer_type> If the current value is too small to fit into the new type.
					 */
					template<class _new_integer_type>
					operator safe_int<_new_integer_type>() const {
						if(value > safe_int<_new_integer_type>::max) {
							throw error_overflow_typed<_new_integer_type>();
						}
						if(value < safe_int<_new_integer_type>::min) {
							throw error_underflow_typed<_new_integer_type>();
						}
						return safe_int<_new_integer_type>((_new_integer_type) value);
					}
					
					/*!
					 * @brief Add somothing else to this integer.
					 * 
					 * @param o The integer that should be added to this integer.
					 * @return *this
					 * @throw dsal::math::error_overflow_typed If the sum would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the sum would be too low for fitting into this integer type.
					 */
					safe_int<integer_type>& operator+=(const safe_int<integer_type>& o) {
						if(o.value > 0) {
							if(value > max - o.value) {
								throw error_overflow_typed<integer_type>();
							}
						} else {
							if(value < min - o.value) {
								throw error_underflow_typed<integer_type>();
							}
						}
						value += o.value;
						return *this;
					}
					
					/*!
					 * @brief Add somothing else to this integer.
					 * 
					 * This function exists mainly for not having any problems when not converting to the correct type.
					 * 
					 * @param o The integer that should be added to this integer.
					 * @return *this
					 * @throw dsal::math::error_overflow_typed If the sum would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the sum would be too low for fitting into this integer type.
					 */
					safe_int<integer_type>& operator+=(integer_type o) {
						return operator+=(safe_int<integer_type>(o));
					}
					
					/*!
					 * @brief Remove somothing from this integer.
					 * 
					 * @param o The integer that should be removed from this integer.
					 * @return *this
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 */
					safe_int<integer_type>& operator-=(const safe_int<integer_type>& o) {
						if(o.value < 0) {
							if(value > max + o.value) {
								throw error_overflow_typed<integer_type>();
							}
						} else {
							if(value < min + o.value) {
								throw error_underflow_typed<integer_type>();
							}
						}
						value -= o.value;
						return *this;
					}
					
					/*!
					 * @brief Remove somothing from this integer.
					 * 
					 * This function exists mainly for not having any problems when not converting to the correct type.
					 * 
					 * @param o The integer that should be removed from this integer.
					 * @return *this
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 */
					safe_int<integer_type>& operator-=(integer_type o) {
						return operator-=(safe_int<integer_type>(o));
					}
					
					/*!
					 * @brief Multiply this with another integer.
					 * 
					 * @param o The integer this should be multiplied by.
					 * @return *this
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 */
					safe_int<integer_type>& operator*=(const safe_int<integer_type>& o) {
						if(value == 0 || o.value == 0) {
							value = 0;
							return *this;
						}
						if(value == -1 && o.value == min) {
							throw error_overflow_typed<integer_type>();
						}
						if(o.value == -1 && value == min) {
							throw error_overflow_typed<integer_type>();
						}
						if(value > max / o.value) {
							throw error_overflow_typed<integer_type>();
						}
						if(value < min / o.value) {
							throw error_underflow_typed<integer_type>();
						}
						value *= o.value;
						return *this;
					}
					
					/*!
					 * @brief Multiply this with another integer.
					 * 
					 * This function exists mainly for not having any problems when not converting to the correct type.
					 * 
					 * @param o The integer this should be multiplied by.
					 * @return *this
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 */
					safe_int<integer_type>& operator*=(integer_type o) {
						return operator*=(safe_int<integer_type>(o));
					}
					
					/*!
					 * @brief Divide this integer by another one.
					 * 
					 * @param o The integer this should be divided by.
					 * @return *this
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 * @throw dsal::math::error_division_by_zero_typed If o == 0
					 */
					safe_int<integer_type>& operator/=(const safe_int<integer_type>& o) {
						if(o.value == 0) {
							throw error_division_by_zero_typed<integer_type>(value);
						}
						if(value == min && o.value == -1) {
							throw error_overflow_typed<integer_type>();
						}
						value /= o.value;
						return *this;
					}
					
					/*!
					 * @brief Divide this integer by another one.
					 * 
					 * This function exists mainly for not having any problems when not converting to the correct type.
					 * 
					 * @param o The integer this should be divided by.
					 * @return *this
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 * @throw dsal::math::error_division_by_zero_typed If o == 0
					 */
					safe_int<integer_type>& operator/=(integer_type o) {
						return operator/=(safe_int<integer_type>(o));
					}
					
					/*!
					 * @brief Calculate the modulo of this and another integer.
					 * 
					 * Modulo is dividing by and taking the rest as the result.
					 * 
					 * @param o The other integer.
					 * @return *this
					 * @throw dsal::math::error_division_by_zero_typed If o == 0
					 */
					safe_int<integer_type>& operator%=(const safe_int<integer_type>& o) {
						if(o.value == 0) {
							throw error_division_by_zero_typed<integer_type>(value);
						}
						value %= o.value;
						return *this;
					}
					
					/*!
					 * @brief Calculate the modulo of this and another integer.
					 * 
					 * This function exists mainly for not having any problems when not converting to the correct type.
					 * 
					 * Modulo is dividing by and taking the rest as the result.
					 * 
					 * @param o The other integer.
					 * @return *this
					 * @throw dsal::math::error_division_by_zero_typed If o == 0
					 */
					safe_int<integer_type>& operator%=(integer_type o) {
						return operator%=(safe_int<integer_type>(o));
					}
					
					/*!
					 * @brief Increase this integer by 1.
					 * 
					 * @return *this
					 * @throw dsal::math::error_overflow_typed If this integer is the maximum.
					 */
					safe_int<integer_type>& operator++() {
						if(value == max) {
							throw error_overflow_typed<integer_type>();
						}
						++value;
						return *this;
					}
					
					/*!
					 * @brief Post-increment operator.
					 * 
					 * Copies this integer first, increase this integer and return the copy.
					 * 
					 * @return A copy of *this before increasing.
					 * @throw dsal::math::error_overflow_typed If this integer is the maximum.
					 */
					safe_int<integer_type> operator++(int) {
						safe_int<integer_type> result = *this;
						if(value == max) {
							throw error_overflow_typed<integer_type>();
						}
						++value;
						return result;
					}
					
					/*!
					 * @brief Decrease this integer by 1.
					 * 
					 * @return *this
					 * @throw dsal::math::error_underflow_typed If this integer is the minimum.
					 */
					safe_int<integer_type>& operator--() {
						if(value == min) {
							throw error_underflow_typed<integer_type>();
						}
						--value;
						return *this;
					}
					
					/*!
					 * @brief Post-decrement operator.
					 * 
					 * Copies this integer first, decrease this integer and return the copy.
					 * 
					 * @return A copy of *this before increasing.
					 * @throw dsal::math::error_underflow_typed If this integer is the maximum.
					 */
					safe_int<integer_type> operator--(int) {
						safe_int<integer_type> result = *this;
						if(value == min) {
							throw error_underflow_typed<integer_type>();
						}
						--value;
						return result;
					}
					
					/*!
					 * @brief Add this and another integer.
					 * 
					 * @param o The other integer.
					 * @return The sum of this and another integer.
					 * @throw dsal::math::error_overflow_typed If the sum would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the sum would be too low for fitting into this integer type.
					 */
					safe_int<integer_type> operator+(const safe_int<integer_type>& o) const {
						safe_int<integer_type> result = *this;
						result += o;
						return result;
					}
					
					/*!
					 * @brief Add this and another integer.
					 * 
					 * This function exists mainly for not having any problems when not converting to the correct type.
					 * 
					 * @param o The other integer.
					 * @return The sum of this and another integer.
					 * @throw dsal::math::error_overflow_typed If the sum would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the sum would be too low for fitting into this integer type.
					 */
					safe_int<integer_type> operator+(integer_type o) const {
						return operator+(safe_int<integer_type>(o));
					}
					
					/*!
					 * @brief Subtract another from this integer.
					 * 
					 * @param o The integer to se subtracted.
					 * @return The difference of o and this.
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 */
					safe_int<integer_type> operator-(const safe_int<integer_type>& o) const {
						safe_int<integer_type> result = *this;
						result -= o;
						return result;
					}
					
					/*!
					 * @brief Subtract another from this integer.
					 * 
					 * This function exists mainly for not having any problems when not converting to the correct type.
					 * 
					 * @param o The integer to se subtracted.
					 * @return The difference of o and this.
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 */
					safe_int<integer_type> operator-(integer_type o) const {
						return operator-(safe_int<integer_type>(o));
					}
					
					/*!
					 * @brief Multiply this by another integer.
					 * 
					 * @param o The integer this should be multiplied by.
					 * @return The product of this integer and o.
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 */
					safe_int<integer_type> operator*(const safe_int<integer_type>& o) const {
						safe_int<integer_type> result = *this;
						result *= o;
						return result;
					}
					
					/*!
					 * @brief Multiply this by another integer.
					 * 
					 * This function exists mainly for not having any problems when not converting to the correct type.
					 * 
					 * @param o The integer this should be multiplied by.
					 * @return The product of this integer and o.
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 */
					safe_int<integer_type> operator*(integer_type o) const {
						return operator*(safe_int<integer_type>(o));
					}
					
					/*!
					 * @brief Divide this integer by another one.
					 * 
					 * @param o The integer this one should be divided by.
					 * @return The quotient of this integer and o.
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 * @throw dsal::math::error_division_by_zero_typed If o == 0
					 */
					safe_int<integer_type> operator/(const safe_int<integer_type>& o) const {
						safe_int<integer_type> result = *this;
						result /= o;
						return result;
					}
					
					/*!
					 * @brief Divide this integer by another one.
					 * 
					 * This function exists mainly for not having any problems when not converting to the correct type.
					 * 
					 * @param o The integer this one should be divided by.
					 * @return The quotient of this integer and o.
					 * @throw dsal::math::error_overflow_typed If the result would be too high for fitting into this integer type.
					 * @throw dsal::math::error_underflow_typed If the result would be too low for fitting into this integer type.
					 * @throw dsal::math::error_division_by_zero_typed If o == 0
					 */
					safe_int<integer_type> operator/(integer_type o) const {
						return operator/(safe_int<integer_type>(o));
					}
					
					/*!
					 * @brief Calculate the modulo of this and another integer.
					 * 
					 * Modulo is dividing by and taking the rest as the result.
					 * 
					 * @param o The other integer.
					 * @return The result of the modulo operation.
					 * @throw dsal::math::error_division_by_zero_typed If o == 0
					 */
					safe_int<integer_type> operator%(const safe_int<integer_type>& o) const {
						safe_int<integer_type> result = *this;
						result %= o;
						return result;
					}
					
					/*!
					 * @brief Calculate the modulo of this and another integer.
					 * 
					 * This function exists mainly for not having any problems when not converting to the correct type.
					 * 
					 * Modulo is dividing by and taking the rest as the result.
					 * 
					 * @param o The other integer.
					 * @return The result of the modulo operation.
					 * @throw dsal::math::error_division_by_zero_typed If o == 0
					 */
					safe_int<integer_type> operator%(integer_type o) const {
						return operator%(safe_int<integer_type>(o));
					}
					
					/*!
					 * @brief Compare this against another integer.
					 * 
					 * @param o The integer this one should be compared to.
					 * @return true if this < o, false otherwise.
					 */
					bool operator<(const safe_int<integer_type>& o) const noexcept {
						return value < o.value;
					}
					
					/*!
					 * @brief Compare this against another integer.
					 * 
					 * @param o The integer this one should be compared to.
					 * @return true if this <= o, false otherwise.
					 */
					bool operator<=(const safe_int<integer_type>& o) const noexcept {
						return value <= o.value;
					}
					
					/*!
					 * @brief Compare this against another integer.
					 * 
					 * @param o The integer this one should be compared to.
					 * @return true if this == o, false otherwise.
					 */
					bool operator==(const safe_int<integer_type>& o) const noexcept {
						return value == o.value;
					}
					
					/*!
					 * @brief Compare this against another integer.
					 * 
					 * @param o The integer this one should be compared to.
					 * @return true if this != o, false otherwise.
					 */
					bool operator!=(const safe_int<integer_type>& o) const noexcept {
						return value != o.value;
					}
					
					/*!
					 * @brief Compare this against another integer.
					 * 
					 * @param o The integer this one should be compared to.
					 * @return true if this >= o, false otherwise.
					 */
					bool operator>=(const safe_int<integer_type>& o) const noexcept {
						return value >= o.value;
					}
					
					/*!
					 * @brief Compare this against another integer.
					 * 
					 * @param o The integer this one should be compared to.
					 * @return true if this > o, false otherwise.
					 */
					bool operator>(const safe_int<integer_type>& o) const noexcept {
						return value > o.value;
					}
					
					/*!
					 * @brief Compare this against another integer.
					 * 
					 * @param o The integer this one should be compared to.
					 * @return true if this > o, false otherwise.
					 */
					bool operator>(integer_type o) const noexcept {
						return value > o;
					}
			};
	}
}

/*!
 * @brief Compare a safe_int against a usual integer.
 * 
 * @param i1 The safe_int.
 * @param i2 The usual integer.
 * @return true if i1 < i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator<(dsal::math::safe_int<_int_t_1> i1, _int_t_2 i2) noexcept {
	if(dsal::math::safe_int<_int_t_1>::min > i2) {
		return false;
	}
	if(dsal::math::safe_int<_int_t_1>::max < i2) {
		return true;
	}
	return i1 < dsal::math::safe_int<_int_t_1>(i2);
}

/*!
 * @brief Compare a usual integer against a safe_int.
 * 
 * @param i1 The usual integer.
 * @param i2 The safe_int.
 * @return true if i1 < i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator<(_int_t_1 i1, dsal::math::safe_int<_int_t_2> i2) noexcept {
	if(i1 < dsal::math::safe_int<_int_t_2>::min) {
		return true;
	}
	if(i1 > dsal::math::safe_int<_int_t_2>::max) {
		return false;
	}
	return dsal::math::safe_int<_int_t_2>(i1) < i2;
}

/*!
 * @brief Compare a safe_int against a usual integer.
 * 
 * @param i1 The safe_int.
 * @param i2 The usual integer.
 * @return true if i1 <= i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator<=(dsal::math::safe_int<_int_t_1> i1, _int_t_2 i2) noexcept {
	if(dsal::math::safe_int<_int_t_1>::min > i2) {
		return false;
	}
	if(dsal::math::safe_int<_int_t_1>::max < i2) {
		return true;
	}
	return i1 <= dsal::math::safe_int<_int_t_1>(i2);
}

/*!
 * @brief Compare a usual integer against a safe_int.
 * 
 * @param i1 The usual integer.
 * @param i2 The safe_int.
 * @return true if i1 <= i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator<=(_int_t_1 i1, dsal::math::safe_int<_int_t_2> i2) noexcept {
	if(i1 < dsal::math::safe_int<_int_t_2>::min) {
		return true;
	}
	if(i1 > dsal::math::safe_int<_int_t_2>::max) {
		return false;
	}
	return dsal::math::safe_int<_int_t_2>(i1) <= i2;
}

/*!
 * @brief Compare a safe_int against a usual integer.
 * 
 * @param i1 The safe_int.
 * @param i2 The usual integer.
 * @return true if i1 == i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator==(dsal::math::safe_int<_int_t_1> i1, _int_t_2 i2) noexcept {
	if(i2 < dsal::math::safe_int<_int_t_1>::min || i2 > dsal::math::safe_int<_int_t_1>::max) {
		return false;
	}
	return i1 == dsal::math::safe_int<_int_t_1>(i2);
}

/*!
 * @brief Compare a usual integer against a safe_int.
 * 
 * @param i1 The usual integer.
 * @param i2 The safe_int.
 * @return true if i1 == i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator==(_int_t_1 i1, dsal::math::safe_int<_int_t_2> i2) {
	if(i1 < dsal::math::safe_int<_int_t_2>::min || i1 > dsal::math::safe_int<_int_t_2>::max) {
		return false;
	}
	return dsal::math::safe_int<_int_t_2>(i1) == i2;
}

/*!
 * @brief Compare a safe_int against a usual integer.
 * 
 * @param i1 The safe_int.
 * @param i2 The usual integer.
 * @return true if i1 != i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator!=(dsal::math::safe_int<_int_t_1> i1, _int_t_2 i2) noexcept {
	if(i2 < dsal::math::safe_int<_int_t_1>::min || i2 > dsal::math::safe_int<_int_t_1>::max) {
		return true;
	}
	return i1 != dsal::math::safe_int<_int_t_1>(i2);
}

/*!
 * @brief Compare a usual integer against a safe_int.
 * 
 * @param i1 The usual integer.
 * @param i2 The safe_int.
 * @return true if i1 != i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator!=(_int_t_1 i1, dsal::math::safe_int<_int_t_2> i2) {
	if(i1 < dsal::math::safe_int<_int_t_2>::min || i1 > dsal::math::safe_int<_int_t_2>::max) {
		return true;
	}
	return dsal::math::safe_int<_int_t_2>(i1) != i2;
}

/*!
 * @brief Compare a safe_int against a usual integer.
 * 
 * @param i1 The safe_int.
 * @param i2 The usual integer.
 * @return true if i1 >= i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator>=(dsal::math::safe_int<_int_t_1> i1, _int_t_2 i2) noexcept {
	if(dsal::math::safe_int<_int_t_1>::min > i2) {
		return true;
	}
	if(dsal::math::safe_int<_int_t_1>::max < i2) {
		return false;
	}
	return i1 >= dsal::math::safe_int<_int_t_1>(i2);
}

/*!
 * @brief Compare a usual integer against a safe_int.
 * 
 * @param i1 The usual integer.
 * @param i2 The safe_int.
 * @return true if i1 >= i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator>=(_int_t_1 i1, dsal::math::safe_int<_int_t_2> i2) noexcept {
	if(i1 < dsal::math::safe_int<_int_t_2>::min) {
		return false;
	}
	if(i1 > dsal::math::safe_int<_int_t_2>::max) {
		return true;
	}
	return dsal::math::safe_int<_int_t_2>(i1) >= i2;
}

/*!
 * @brief Compare a safe_int against a usual integer.
 * 
 * @param i1 The safe_int.
 * @param i2 The usual integer.
 * @return true if i1 > i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator>(dsal::math::safe_int<_int_t_1> i1, _int_t_2 i2) noexcept {
	if(dsal::math::safe_int<_int_t_1>::min > i2) {
		return true;
	}
	if(dsal::math::safe_int<_int_t_1>::max < i2) {
		return false;
	}
	return i1 > dsal::math::safe_int<_int_t_1>(i2);
}

/*!
 * @brief Compare a usual integer against a safe_int.
 * 
 * @param i1 The usual integer.
 * @param i2 The safe_int.
 * @return true if i1 > i2, false otherwise.
 * @tparam _int_t_1 The integer type of i1.
 * @tparam _int_t_2 The integer type of i2.
 */
template<class _int_t_1, class _int_t_2>
bool operator>(_int_t_1 i1, dsal::math::safe_int<_int_t_2> i2) noexcept {
	if(i1 < dsal::math::safe_int<_int_t_2>::min) {
		return false;
	}
	if(i1 > dsal::math::safe_int<_int_t_2>::max) {
		return true;
	}
	return dsal::math::safe_int<_int_t_2>(i1) > i2;
}

#endif
