/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_MATH_ERROR_DIVISION_BY_ZERO_TYPED_HPP
#define DSAL_MATH_ERROR_DIVISION_BY_ZERO_TYPED_HPP

/*!
 * @file
 * @brief The file defining the dsal::math::error_division_by_zero_typed template error.
 */

#include "error_division_by_zero.hpp"

namespace dsal {
	namespace math {
		/*!
		 * @brief Division by zero.
		 * 
		 * Division by zero is not even defined in math.
		 * 
		 * This is an error interface, meaning you shouldn't throw it directly. It is there so you can catch a group of errors with a single catch statement.
		 * This interface is for every case where a number gets divided by zero, independent of the type of the number.
		 */
		template<class _int_t>
			class error_division_by_zero_typed : public error_division_by_zero {
				public:
					/*!
					 * @brief The integer type that was too small.
					 */
					typedef _int_t integer_type;
					
					/*!
					 * @brief The value the application tried to divide by zero.
					 */
					integer_type value;
					
					/*!
					 * @brief Initialize the error.
					 * 
					 * @param v The value that was tried to divide by zero.
					 */
					error_division_by_zero_typed(const integer_type& v) : value(v) {}
					
					virtual const char* what_dev() const noexcept override {
						return
							"error type: dsal::math::error_division_by_zero_typed\n"
							"<No details availible>\n";
					}
					virtual const char* what_code_dev() const noexcept override {
						return "dsal::math::error_division_by_zero_typed";
					}
			};
	}
}

#endif
