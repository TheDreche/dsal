/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_DATA_HPP
#define DSAL_DATA_HPP

/*!
 * @file
 * @brief A file for quickly including all more complex base data types.
 * 
 * This means classes such as lists or hash maps.
 */

namespace dsal {
	/*!
	 * @brief High level data structures.
	 */
	namespace data {}
}

/* For vim/gvim users:
 * 
 * You can update this file easily using
 * 
 * 	:read !find include/dsal/data/ -maxdepth 1 -name '*.hpp' | sort | sed 's+include/dsal/+\#include "+;s+$+"+'
 */

#include "data/abstract.hpp"
#include "data/array.hpp"
#include "data/callback.hpp"

#include "data/error.hpp"
#include "data/error_index_out_of_bounds.hpp"

#endif
