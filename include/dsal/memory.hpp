/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_MEMORY_HPP
#define DSAL_MEMORY_HPP

/*!
 * @file
 * @brief A file for quickly including all memory management functions.
 * 
 * @sa dsal::memory
 */

namespace dsal {
	/*!
	 * @brief Low level memory management classes/functions.
	 * 
	 * This means functions for constructing/destructing objects, memory sizes and differences, and very low level dynamically allocated data types.
	 * 
	 * For the last point, there is @ref dsal::memory::array, which is a low level version of @ref dsal::data::array, not caring about content initialization or destruction, but being on the heap, thus only requiring a compile time known data type and not the size.
	 * 
	 * There are no allocation functions as you should not need one: The only thing you may ever need is a dynamically allocated memory resource. Having them as separate classes makes memory leaks appear less, as you can't forget calling the deallocation functions because an exception has been thrown.
	 * 
	 * There are several needs for a dynamically allocated memory resource, I try to have a class for each one. If you find one missing, report it as a bug. Currently, there is @ref dsal::memory::array and @ref dsal::data::abstract.
	 * 
	 * I noticed, independent of the actual usage, you need to know the interface you plan on using. So, only @ref dsal::data::abstract came me into mind. However, later I noticed that you maybe also need a resizable array.
	 * 
	 * In C++, arrays are implemented completely separate from templates, because of technical limitations at one end. However, arrays actually are templated types: one template parameter specifies the type, and one specifies _the length_. You can allocate arrays of runtime known length, but you can't allocate space vor values whose template parameters are only known at runtime. So, arrays are like templated classes, but are implemented differently.
	 * 
	 * I had to find a solution to this problem and thought that if C++ handles arrays completely different, why don't I just follow that? I did, and now there is a completely separate class for them: @ref dsal::memory::array. As this is part of the memory section, it is kept as low level as possible without exposing the actual allocation or its parameters. Even bytes are considered an internal part of this, so even they shouldn't get exposed.
	 * 
	 * Because all of the above paragraphs, the choice of having a class for abstract types and one for dynamically allocated arrays isn't arbitrary.
	 */
	namespace memory {}
}

/* For vim/gvim users:
 * 
 * You can update this file easily using
 * 
 * 	:read !find include/dsal/memory/ -maxdepth 1 -name '*.hpp' | sort | sed 's+include/dsal/+\#include "+;s+$+"+'
 */

#include "memory/array.hpp"
#include "memory/construct.hpp"
#include "memory/destruct.hpp"
#include "memory/size.hpp"
#include "memory/size_difference.hpp"

#include "memory/error.hpp"
#include "memory/error_memory_not_enough.hpp"

#endif
