/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_DATA_CALLBACK_HPP
#define DSAL_DATA_CALLBACK_HPP

/*!
 * @file
 * @brief The file defining the dsal::data::callback class.
 */

#include <utility> // std::move, std::forward

#include "abstract.hpp"

namespace dsal {
	namespace data {
		template<class _func_sig>
		class callback;
		
		/*!
		 * @brief A callback.
		 * 
		 * A callback is a function doing something out of the range for the function. It is essentially a function pointer, but this type has some extra features.
		 * 
		 * @tparam _return_t The return type of the callback.
		 * @tparam _input_t The types of the input parametres.
		 */
		template<class _return_t, class... _input_t>
		class callback<_return_t(_input_t...)> {
			public:
				/*!
				 * @brief A class every closure parameter type should inherit from.
				 */
				class data {
					public:
						virtual ~data() {}
				};
				
			public:
				/*!
				 * @brief The closure parameter type.
				 * 
				 * This should be the first parameter of the function being called.
				 */
				typedef dsal::data::abstract<data>& closure_parameter_type;
				
				/*!
				 * @brief The return type of the function.
				 * 
				 * The callback will return a value of this type.
				 */
				typedef _return_t result_type;
				
				/*!
				 * @brief The type of a function for this.
				 * 
				 * A function taking the correct arguments and returning the correct value.
				 */
				typedef result_type(function)(_input_t...);
				
				/*!
				 * @brief The type of a function pointer for this.
				 * 
				 * A function pointer taking the correct arguments and returning the correct value.
				 */
				typedef result_type(*function_pointer)(_input_t...);
				
				/*!
				 * @brief The type of a function for this having static data.
				 * 
				 * A function returning the correct value and taking the static data and the correct arguments.
				 */
				typedef result_type(data_function)(closure_parameter_type, _input_t...);
				
				/*!
				 * @brief The type of a function pointer for this having static data.
				 * 
				 * A function pointer returning the correct value and taking the static data and the correct arguments.
				 */
				typedef result_type(*data_function_pointer)(closure_parameter_type, _input_t...);
				
				/*!
				 * @brief The type of this callback.
				 */
				typedef callback<_return_t(_input_t...)> type;
				
				/*!
				 * @brief Closure parameter.
				 * 
				 * This will be passed as the first parameter to the function.
				 */
				abstract<data> closure;
				
				/*!
				 * @brief The actual function.
				 * 
				 * That function is called when the callback is called. It's first argument is the data and the other are the input parametres.
				 */
				data_function_pointer func = nullptr;
				
			private:
				class function_saver : public data {
					public:
						function_pointer func;
						function_saver(function_pointer func) : func(func) {}
				};
				
				static result_type call_func(closure_parameter_type func, _input_t... params) {
					return ((function_saver*)(data*) func)->func(std::forward<_input_t>(params)...);
				}
				
				template<class _func_t>
				class anyfunc_saver : public data {
					public:
						_func_t func;
						anyfunc_saver(const _func_t& func) : func(func) {}
				};
				
				template<class _func_t>
				static result_type call_anyfunc(closure_parameter_type func, _input_t... params) {
					return ((anyfunc_saver<_func_t>*)(data*) func)->func(std::forward<_input_t>(params)...);
				}
			public:
				/*!
				 * @brief Initialize noop.
				 * 
				 * A callback doing nothing.
				 */
				callback() : closure(data()) {}
				
				/*!
				 * @brief Set this to a function.
				 * 
				 * @param o The function to be called.
				 */
				callback(function_pointer o) : closure(data()) {
					*this = o;
				}
				
				/*!
				 * @brief Set this to any callable object.
				 * 
				 * @tparam _func_t The callable object type.
				 * @param func The function to be called.
				 */
				template<class _func_t>
				callback(_func_t func) : closure(data()) {
					*this = func;
				}
				
				/*!
				 * @brief Copy constructor.
				 * 
				 * @param o The object to be copyed.
				 */
				callback(const type& o) : func(o.func), closure(o.closure) {}
				
				/*!
				 * @brief Move constructor.
				 * 
				 * @param o The object to be moved.
				 */
				callback(type&& o) : closure(data()) {
					*this = std::move(o);
				}
				
				/*!
				 * @brief Copy assignment operator.
				 * 
				 * @param o The callback to set this to.
				 * @return The new *this.
				 */
				type& operator=(const type& o) {
					this->func = nullptr;
					this->closure = o.closure;
					this->func = o.func;
					return *this;
				}
				
				/*!
				 * @brief Move assignment operator.
				 * 
				 * @param o The callback to set this to.
				 * @return The new *this.
				 */
				type& operator=(type&& o) {
					this->func = nullptr;
					this->closure = std::move(o.closure);
					this->func = o.func;
					o.func = nullptr;
					return *this;
				}
				
				/*!
				 * @brief Set to a function.
				 * 
				 * @param new_func The function to be called.
				 * @return The new *this.
				 */
				type& operator=(function_pointer new_func) {
					this->func = nullptr;
					this->closure = function_saver{new_func};
					this->func = &call_func;
					return *this;
				}
				
				/*!
				 * @brief Set to any callable object.
				 * 
				 * @tparam _func_t The type of the callable object.
				 * @param new_func The callable object.
				 * @return The new *this.
				 */
				template<class _func_t>
				type& operator=(const _func_t& new_func) {
					this->func = nullptr;
					this->closure = anyfunc_saver<_func_t>{new_func};
					this->func = &call_anyfunc<_func_t>;
					return *this;
				}
				
				/*!
				 * @brief Call the callback.
				 * 
				 * @param args The arguments to give to the function.
				 * @return The return value.
				 */
				result_type call(_input_t... args) {
					return func(closure, std::forward<_input_t>(args)...);
				}
				
				/*!
				 * @brief Call the callback.
				 * 
				 * @param args The arguments to give to the function.
				 * @return The return value.
				 */
				result_type operator()(_input_t... args) {
					return call(std::forward<_input_t>(args)...);
				}
				
				/*!
				 * @brief Whether this callback is empty.
				 * 
				 * It is empty when it waits for having a function assigned. The function is then nullptr and a try to call it will result in undefined behavior, probably a segmentation fault.
				 * 
				 * @return Whther this is empty and doesn't have a function.
				 */
				bool is_empty() const {
					return func == nullptr;
				}
				
				~callback() {
					this->func = nullptr;
				}
		};
	}
}

#endif
