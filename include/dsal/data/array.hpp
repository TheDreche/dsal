/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_DATA_ARRAY_HPP
#define DSAL_DATA_ARRAY_HPP

/*!
 * @file
 * @brief The file defining the @ref dsal::data::array class.
 */

#include "../memory/construct.hpp"
#include "../memory/size.hpp"
#include "../memory/size_difference.hpp"
#include "../concepts/random_access_iterator.hpp"

namespace dsal {
	namespace data {
		/*!
		 * @brief A static array.
		 * 
		 * This is an array of elements, allocated on the stack.
		 * 
		 * It has some improvements over usual arrays (type value[length]):
		 * 1. It saves the length of the array.
		 * 2. You can assign whole arrays to another.
		 * 3. You can initialitze/assign this array using an iterator.
		 * The last two points only apply if the type is copy constructible/assignable.
		 * 
		 * The const qualifier for this array specifies nothing: The array itself can't be modified either way, only the content can. Thus, if you want to have an array of constant items, make the Type template parameter const qualified.
		 * 
		 * All elements of this array will be initialitzed at construction.
		 * 
		 * @tparam Type The item type.
		 * @tparam Length The item count.
		 */
		template<class Type, dsal::memory::size Length>
		class array final {
			public:
				/*!
				 * @brief The type of the items.
				 */
				typedef Type item;
				
				/*!
				 * @brief The length of an array of this type.
				 */
				static const dsal::memory::size length = Length;
				
			private:
				/*!
				 * @internal
				 * @brief The actual content.
				 */
				mutable item content[length];
				
			public:
				/*!
				 * @brief An iterator through an array of this type.
				 * 
				 * Since it is easy and intended by the definition, it is a random access iterator.
				 */
				class iterator final : public dsal::concepts::random_access_iterator<item> {
					private:
						/*!
						 * @internal
						 * @brief The item this iterator is at.
						 */
						item* at;
						iterator(item* at) : at(at) {}
						friend array<item, length>;
					public:
						// iterator
						item* pointer() const final override {
							return this->at;
						}
						iterator& operator++() final override {
							this->next();
							return *this;
						}
						/*!
						 * @brief Move on to the next item.
						 * 
						 * @return A copy of this iterator before incrementing.
						 */
						iterator operator++(int) {
							iterator r = *this;
							this->next();
							return r;
						}
						// bidirectional iterator
						iterator& operator--() final override {
							this->previous();
							return *this;
						}
						/*!
						 * @brief Move back to the previous item.
						 * 
						 * @return A copy of this before decrementing.
						 */
						iterator operator--(int) {
							iterator r = *this;
							this->previous();
							return r;
						}
						// random access iterator
						void change(dsal::memory::size_difference offset) final override {
							this->at += offset;
						}
						iterator& operator+=(dsal::memory::size_difference offset) final override {
							this->move_forward(offset);
							return *this;
						}
						iterator& operator-=(dsal::memory::size_difference offset) final override {
							this->move_back(offset);
							return *this;
						}
						/*!
						 * @brief Get the iterator some positions further.
						 * 
						 * If the offset is negative, go back instead. If zero, return a copy of this iterator.
						 * 
						 * @param offset The number of items to skip.
						 * @return An iterator offset items further.
						 */
						iterator operator+(dsal::memory::size_difference offset) {
							return iterator(this->at + offset);
						}
						/*!
						 * @brief Get the iterator some positions earlier.
						 * 
						 * If the offset is negative, go forward instead. If zero, return a copy of this iterator.
						 * 
						 * @param offset The number of items to go back.
						 * @return An iterator offset items earlier.
						 */
						iterator operator-(dsal::memory::size_difference offset) {
							return iterator(this->at - offset);
						}
						/*!
						 * @brief Compare the two iterators.
						 * 
						 * An iterator is smaller if you can increase it some times to get the other iterator.
						 * 
						 * It is undefined what would be returned if the iterators result from a different array.
						 * 
						 * @param o The iterator to compare this one to.
						 */
						bool operator<(const iterator& o) const {
							return this->at < o.at;
						}
						/*!
						 * @copydoc operator<(const iterator& o) const
						 */
						bool operator<=(const iterator& o) const {
							return this->at <= o.at;
						}
						/*!
						 * @copydoc operator<(const iterator& o) const
						 */
						bool operator==(const iterator& o) const {
							return this->at == o.at;
						}
						/*!
						 * @copydoc operator<(const iterator& o) const
						 */
						bool operator>=(const iterator& o) const {
							return this->at >= o.at;
						}
				};
			public:
				/*!
				 * @brief An iterator to the beginning.
				 * 
				 * @return An iterator beginning at the first element.
				 */
				iterator begin() const {
					return iterator(&content[0]);
				}
				/*!
				 * @brief Past end iterator.
				 * 
				 * @return An iterator being one element past the last one.
				 */
				iterator end() const {
					return iterator(&content[length]);
				}
				/*!
				 * @brief Default construct all elements.
				 */
				array() : content{} {}
				/*!
				 * @brief Copy the elements from an iterator.
				 * 
				 * Because of c++ limitations, the array element type has to be copy assignable for using this (in specific, there is no way not to initialitze array members).
				 * 
				 * The given iterator will be increased for every item.
				 * 
				 * @param iter The iterator to copy the elements from.
				 */
				array(dsal::concepts::iterator<item>& iter) {
					for(item& i : *this) {
						i = *iter;
						iter.next();
					}
				}
				
				/*!
				 * @brief The size of this array.
				 * 
				 * @return The size of this array, in items.
				 */
				dsal::memory::size size() const {
					return length;
				}
				
				/*!
				 * @brief Assign the elements to the ones from an iterator.
				 * 
				 * This copies the elements as returned by an iterator into this array.
				 * 
				 * The iterator will be increased for every element.
				 * 
				 * @param iter Reference to the iterator to read the elements from.
				 */
				void assign(dsal::concepts::iterator<item>& iter) const {
					for(item& i : *this) {
						i = *iter;
						iter.next();
					}
				}
				/*!
				 * @brief Assign this array to another one.
				 * 
				 * This copies the elements of another array into this one.
				 * 
				 * @param o The array to copy its elements from.
				 */
				void assign(const array<item, length>& o) const {
					iterator iter = o.begin();
					assign(iter);
				}
				/*!
				 * @copydoc assign(const array<item, length>& o) const
				 * @return A reference to this array.
				 */
				const array<item, length>& operator=(const array<item, length>& o) const {
					assign(o);
					return *this;
				}
				
				/*!
				 * @brief Access the specified element.
				 * 
				 * No bound checking is being done.
				 * 
				 * @param index The index of the element to get a reference to, counting from 0.
				 * @return A reference to the specified element.
				 */
				item& operator[](dsal::memory::size index) const {
					return content[index];
				}
		};
	}
}

#endif
