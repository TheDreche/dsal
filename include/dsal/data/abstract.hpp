/* Copyright (C) 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_DATA_ABSTRACT_HPP
#define DSAL_DATA_ABSTRACT_HPP

/*!
 * @file
 * @brief The file defining the @ref dsal::data::abstract class.
 */

#include <cstdlib>

#include <new>
#include <typeinfo>
#include <utility>

#include "../memory/_internal/stdallocerr.hpp"
#include "../memory/destruct.hpp"

namespace dsal {
	namespace data {
		/*!
		 * @brief Manager for objects of abstract classes.
		 * 
		 * If you have a class several other classes may inherit from, this class can be quite handy. It manages memory and calling the destructor of such objects.
		 * 
		 * @tparam _A The type several classes may inherit from.
		 */
		template<class _A>
		class abstract final {
			public:
				/*!
				 * @brief The abstract type being managed.
				 */
				typedef _A abstract_type;
			private:
				class base {
					public:
						bool haveSameType(const base& o) const {
							return typeid(*this) == typeid(o);
						}
						virtual base* clone() const = 0;
						virtual abstract_type& operator*() = 0;
						virtual const abstract_type& operator*() const = 0;
						virtual ~base() {}
				};
				template<class _F>
				class abstract_manager final : public base {
					public:
						typedef _F type;
						typedef abstract_manager<type> abstract_manager_type;
					private:
						type self;
					public:
						abstract_manager(const type& o) : self(o) {}
						abstract_manager(type&& o) : self(std::move(o)) {}
						virtual base* clone() const final override {
							abstract_manager_type* r = dsal::memory::_internal::stdalloc_chk_err<abstract_manager_type>(std::malloc(sizeof(abstract_manager_type)));
							new(r) abstract_manager_type(*this);
							return r;
						}
						virtual abstract_type& operator*() final override {
							return self;
						}
						virtual const abstract_type& operator*() const final override {
							return self;
						}
						virtual ~abstract_manager() final override {}
				};
				base* self;
			public:
				/*!
				 * @brief Default-construct to undefined state.
				 * 
				 * The only thing that is defined is that the calls to the destructor and operator= will work properly.
				 */
				abstract() : self(nullptr) {}
				
				/*!
				 * @brief Initialize from an existing object.
				 * 
				 * @tparam _T The type of the existing object.
				 * @param o The existing object.
				 */
				template<class _T>
				abstract(const _T& o) : self(dsal::memory::_internal::stdalloc_chk_err<abstract_manager<_T>>(std::malloc(sizeof(abstract_manager<_T>)))) {
					new(self) abstract_manager<_T>(o);
				}
				
				/*!
				 * @brief Initialize from an existing object.
				 * 
				 * @tparam _T The type of the existing object.
				 * @param o The existing object.
				 */
				template<class _T>
				abstract(const _T&& o) : self(dsal::memory::_internal::stdalloc_chk_err<abstract_manager<_T>>(std::malloc(sizeof(abstract_manager<_T>)))) {
					new(self) abstract_manager<_T>(std::move(o));
				}
				
				/*!
				 * @brief Copy constructor.
				 * 
				 * @param o The existing abstract object.
				 */
				abstract(const abstract<abstract_type>& o) : self(o.self->clone()) {}
				
				/*!
				 * @brief Move constructor.
				 * 
				 * @param o The existing abstract object.
				 */
				abstract(abstract<abstract_type>&& o) : self(o.self) {
					o.self = nullptr;
				}
				
				~abstract() {
					if(self != nullptr) {
						dsal::memory::destruct(self);
						std::free(self);
						self = nullptr;
					}
				}
				
				/*!
				 * @brief Acces a member of this.
				 * 
				 * @return A pointer to the actual object.
				 */
				abstract_type* operator->() {
					return &**self;
				}
				/*!
				 * @brief Acces a member of this without changing anything.
				 * 
				 * @return A pointer to the unmodifiable actual object.
				 */
				const abstract_type* operator->() const {
					return &**self;
				}
				
				/*!
				 * @brief Access the actual object.
				 * 
				 * @return A reference to the actual object.
				 */
				abstract_type& operator*() {
					return *self;
				}
				/*!
				 * @brief Access the actual object without changing anything.
				 * 
				 * @return A reference to the unmodifiable actual object.
				 */
				const abstract_type& operator*() const {
					return *self;
				}
				
				/*!
				 * @brief Set this to a new value.
				 * 
				 * @param o The new value.
				 */
				abstract<abstract_type>& operator=(const abstract<abstract_type>& o) {
					if(self != nullptr) {
						dsal::memory::destruct(self);
						std::free(self);
					}
					self = o.self->clone();
					return *this;
				}
				/*!
				 * @brief Set this to a new value.
				 * 
				 * This uses up the give value.
				 * 
				 * @param o The new value.
				 */
				abstract<abstract_type>& operator=(abstract<abstract_type>&& o) {
					if(self != nullptr) {
						dsal::memory::destruct(self);
						std::free(self);
					}
					self = o.self;
					o.self = nullptr;
					return *this;
				}
				/*!
				 * @brief Set this to a new value.
				 * 
				 * @param o The new value.
				 */
				template<class _T>
				abstract<abstract_type>& operator=(const _T& o) {
					if(self != nullptr) {
						dsal::memory::destruct(self);
					}
					self = dsal::memory::_internal::stdalloc_chk_err<abstract_manager<_T>>(std::realloc(self, sizeof(abstract_manager<_T>)));
					new(self) abstract_manager<_T>(o);
					return *this;
				}
				/*!
				 * @brief Set this to a new value.
				 * 
				 * This uses up the give value.
				 * 
				 * @param o The new value.
				 */
				template<class _T>
				abstract<abstract_type>& operator=(_T&& o) {
					if(self != nullptr) {
						dsal::memory::destruct(self);
					}
					self = dsal::memory::_internal::stdalloc_chk_err<abstract_manager<_T>>(std::realloc(self, sizeof(abstract_manager<_T>)));
					new(self) abstract_manager<_T>(std::move(o));
					return *this;
				}
				
				/*!
				 * @brief Call a function if two values have the same type.
				 * 
				 * If this and other have the same actual type, call func with args. Else, return not_same_types.
				 * 
				 * @tparam function_type The class of the function to call.
				 * @tparam return_type The type returned by the function.
				 * @tparam args The types of the arguments taken by the function.
				 * @param other The object that maybe has the same type as this one.
				 * @param func The function to call if both have the same type.
				 * @param args The arguments to give to func.
				 * @param not_same_types The value to return if this and other have a different type.
				 * @return The return value of the function if both have the same type, not_same_types otherwise.
				 */
				template<class function_type, class return_type, class... param_types>
				return_type callIfSameTypeElse(const abstract<abstract_type>& other, const function_type& func, const param_types&... args, const return_type& not_same_types) {
					if(self->haveSameType(other)) {
						return func(std::forward<param_types>(args)...);
					} else {
						return not_same_types;
					}
				}
				
				/*!
				 * @brief Convert to a regular pointer.
				 */
				operator abstract_type*() {
					return &**self;
				}
				
				/*!
				 * @brief Convert to a regular const pointer.
				 */
				operator const abstract_type*() const {
					return &**self;
				}
		};
	}
}

#endif
