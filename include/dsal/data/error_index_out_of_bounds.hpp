/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_DATA_ERROR_INDEX_OUT_OF_BOUNDS_HPP
#define DSAL_DATA_ERROR_INDEX_OUT_OF_BOUNDS_HPP

/*!
 * @file
 * @brief The file defining the @ref dsal::data::error_index_out_of_bounds error.
 */

#include "error.hpp"
#include "../memory/array.hpp"
#include "../memory/size.hpp"

namespace dsal {
	namespace data {
		/*!
		 * @brief Index out of bounds.
		 * 
		 * This should be thrown when in an array-alike data structure (such as dynamic arrays or lists) an element was tried to access that is higher than the current length.
		 */
		class error_index_out_of_bounds : public error {
			protected:
				/*!
				 * @brief Location to the last allocated string returned by what_dev.
				 * 
				 * If not nullptr will get cleaned up sometime.
				 */
				mutable dsal::memory::array<char> what_dev_result;
				
			public:
				/*!
				 * @brief The size of the container.
				 */
				memory::size size;
				
				/*!
				 * @brief The index that was tried to access.
				 */
				memory::size index;
				
				/*!
				 * @brief Construct an error_index_out_of_bounds.
				 * 
				 * @param size The size of the container.
				 * @param index The index tried to access in the container.
				 */
				error_index_out_of_bounds(memory::size size, memory::size index);
				~error_index_out_of_bounds();
				virtual const char* what() const noexcept override;
				virtual const char* what_localized() const override;
				virtual const char* what_dev() const noexcept override;
				virtual const char* what_code() const noexcept override;
				virtual const char* what_code_localized() const override;
				virtual const char* what_code_dev() const noexcept override;
		};
	}
}

#endif
