/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_ERROR_ERRORINFO_HPP
#define DSAL_ERROR_ERRORINFO_HPP

/*!
 * @file
 * @brief The file defining the whole @ref dsal::error::errorinfo toolset.
 */

#include <cstring>

#include "../math/safe_int.hpp"
#include "../memory/array.hpp"
#include "../memory/size.hpp"

namespace dsal {
	namespace error {
		/*!
		 * @brief The full errorinfo toolset.
		 * 
		 * It provides utilities for easily creating exception types.
		 */
		namespace errorinfo {
			/*!
			 * @brief Get the count of characters required for a string.
			 * 
			 * @param c The string you want to reserve space for.
			 * @return The space needed for the string (without \0).
			 */
			memory::size info_len(const char* c) noexcept;
			
			/*!
			 * @brief Get the count of characters required for an integer.
			 * 
			 * @param i The integer you want to reserve space for.
			 * @return The space needed for the integer (without \0).
			 */
			memory::size info_len(int i) noexcept;
			
			/*!
			 * @brief Get the count of characters required for a dsal::memory::size.
			 * 
			 * @param i The dsal::memory::size you want to reserve space for.
			 * @return The space needed for the dsal::memory::size (without \0).
			 */
			memory::size info_len(memory::size i) noexcept;
			
			/*!
			 * @brief Write a string to another string.
			 * 
			 * to will be pointing to the new end.
			 * 
			 * @param c The string.
			 * @param to Where to write the string.
			 */
			void info_str(const char*const& c, char*& to) noexcept;
			
			/*!
			 * @brief Write an integer to a string.
			 * 
			 * to will be pointing to the new end.
			 * 
			 * @param i The integer.
			 * @param to Where to write the integer to.
			 */
			void info_str(const int& i, char*& to) noexcept;
			
			/*!
			 * @brief Write a dsal::memory::size to a string.
			 * 
			 * to will be pointing to the new end.
			 * 
			 * @param i The dsal::memory::size.
			 * @param to Where to write the dsal::memory::size to.
			 */
			void info_str(const memory::size& i, char*& to) noexcept;
			
			/*!
			 * @brief A piece of error information.
			 * 
			 * @tparam _T The type of the error information.
			 */
			template<class _T>
			class info {
				public:
					/*!
					 * @brief The type of the error information.
					 */
					typedef _T info_type;
					
					/*!
					 * @brief The const type of the error information.
					 */
					typedef const info_type& info_reference;
					
					/*!
					 * @brief The label.
					 * 
					 * It should describe what the information is about in a single word.
					 */
					const char* label;
					
					/*!
					 * @brief The actual information.
					 */
					info_reference data;
					
					/*!
					 * @brief Construct the information.
					 * 
					 * @param label A human-readable name of the information.
					 * @param info The actual information.
					 */
					constexpr info(const char* label, const info_type& info) : label(label), data(info) {}
			};
			
			namespace _internal {
				template<class _current, class... _info_types>
				class count_left_max_len {
					public:
						static inline memory::size action(const info<_current>& current, const info<_info_types>&... other) {
							memory::size other_len = _internal::count_left_max_len<_info_types...>::action(other...);
							memory::size my_len = std::strlen(current.label);
							if(my_len > other_len) {
								return my_len;
							} else {
								return other_len;
							}
						}
				};
				
				template<class _current>
				class count_left_max_len<_current> {
					public:
						static inline constexpr memory::size action(const info<_current>& current) {
							return std::strlen(current.label);
						}
				};
				
				template<class _counted, class... _template_classes>
				class count_template_classes {
					public:
						static constexpr const memory::size length = _internal::count_template_classes<_template_classes...>::length + 1;
				};
				
				template<class _counted>
				class count_template_classes<_counted> {
					public:
						static constexpr const memory::size length = 1;
				};
				
				template<class _current, class... _info_types>
				class count_extra_chars {
					public:
						static inline constexpr memory::size action(const info<_current>& current, const info<_info_types>&... other) {
							return info_len(current.data) + _internal::count_extra_chars<_info_types...>::action(other...);
						}
				};
				
				template<class _current>
				class count_extra_chars<_current> {
					public:
						static inline constexpr memory::size action(const info<_current>& current) {
							return info_len(current.data);
						}
				};
				
				template<class _current, class... _other>
				class append_information {
					public:
						static inline void action(memory::size left_max_len, char* at, const info<_current>& current, const info<_other>&... other) {
							std::strcpy(at, current.label);
							at += std::strlen(current.label);
							for(memory::size i = 0; i < left_max_len - std::strlen(current.label); ++i) {
								*(at++) = ' ';
							}
							*(at++) = ':';
							*(at++) = ' ';
							info_str(current.data, at);
							*(at++) = '\n';
							_internal::append_information<_other...>::action(left_max_len, at, other...);
						}
				};
				
				template<class _current>
				class append_information<_current> {
					public:
						static inline void action(memory::size left_max_len, char* at, const info<_current>& current) {
							std::strcpy(at, current.label);
							at += std::strlen(current.label);
							for(memory::size i = 0; i < left_max_len - std::strlen(current.label); ++i) {
								*(at++) = ' ';
							}
							*(at++) = ':';
							*(at++) = ' ';
							info_str(current.data, at);
							*(at++) = '\n';
							*(at++) = '\0';
						}
				};
			}
			
			/*!
			 * @brief Collect error information.
			 * 
			 * This should make it easy to write errors.
			 * 
			 * If you want to have deatailed error information in dsal::error::error::what_dev, just write
			 * 
			 *	 dsal::error::errorinfo::collect(
			 *		 dsal::error::errorinfo::info<dsal::size>("Needed", needed),
			 *		 dsal::error::errorinfo::info<char*>("Description", strerror(error_number))
			 *	 )
			 * 
			 * The information gets added using the dsal::error::errorinfo::info_len and dsal::error::errorinfo::error_str functions. Add your own functions to support your own classes. The info_len function should return the length of the string needed for the first parametre and the info_str function should append the string to it's second parametre.
			 * 
			 * An example implementation of both functions is helpful. The following is an implementation for a const char*:
			 * 
			 *	 dsal::memory::size info_len(const char* c) noexcept {
			 *		 return std::strlen(c);
			 *	 }
			 *	 void info_str(const char*const& c, char*& to) noexcept {
			 *		 std::strcpy(to, c);
			 *		 to += std::strlen(c);
			 *	 }
			 * 
			 * info_str is required to be noexcept, but info_len actually is allowed to throw an exception. In that case, you have to catch the exception in your what_dev function.
			 * 
			 * This will be the most part. Keep in mind that you have to have an alternative not being able to have any problems as this has to allocate memory which might fail. This will throw an exception if such an error occurrs. Make sure to add a catch for all exceptions.
			 * 
			 * You have to make sure the result gets freed (using dsal::memory::free).
			 * 
			 * A full example implementation of the what_dev method:
			 * 
			 *	const char* error_index_out_of_bounds::what_dev() const noexcept {
			 *		if(what_dev_result != nullptr) {
			 *			return what_dev_result;
			 *		}
			 *		const char* result = "";
			 *		try {
			 *			what_dev_result = dsal::error::errorinfo::collect(
			 *				dsal::error::errorinfo::info<dsal::memory::size>("Actual size", size),
			 *				dsal::error::errorinfo::info<dsal::memory::size>("Accessed element", index)
			 *			);
			 *			result = what_dev_result;
			 *		} catch(dsal::memory::error_memory_not_enough) {
			 *			result =
			 *				"error type: dsal::data::error_index_out_of_bounds\n"
			 *				"<While fetching data: dsal::memory::error_memory_not_enough>\n";
			 *		} catch(...) {
			 *			result =
			 *				"error type: dsal::data::error_index_out_of_bounds\n"
			 *				"<An error occurred while fetching data>\n";
			 *		}
			 *		return result;
			 *	}
			 * 
			 * The above implementation caches the result in the mutable member variable what_dev_result which gets free'd in the destructor of the object.
			 * 
			 * @param information The information to collect.
			 * @return A string containing the information or nullptr on error.
			 */
			template<class... _info_types>
			memory::array<char> collect(const info<_info_types>&... information) {
				math::safe_int<memory::size> extra = (math::safe_int<memory::size>) _internal::count_extra_chars<_info_types...>::action(information...) + (math::safe_int<memory::size>) 1 /* '\0' */;
				math::safe_int<memory::size> left_max_len = _internal::count_left_max_len<_info_types...>::action(information...) + (math::safe_int<memory::size>) 3 /* ':', ' ', '\n' */;
				memory::size length = _internal::count_template_classes<_info_types...>::length;
				
				memory::size need_full = length * left_max_len + extra;
				
				memory::array<char> reserved = memory::array<char>(need_full);
				
				_internal::append_information<_info_types...>::action(left_max_len - (math::safe_int<memory::size>) 3, reserved, information...);
				
				return reserved;
			}
		}
	}
}

#endif
