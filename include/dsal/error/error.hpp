/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_ERROR_ERROR_HPP
#define DSAL_ERROR_ERROR_HPP

/*!
 * @file
 * @brief A file defining the @ref dsal::error::error class.
 */

namespace dsal {
	namespace error {
		/*!
		 * @brief An error.
		 * 
		 * This is a definition requiring many texts:
		 * 
		 * The what text is for having a fallback for what_localized.
		 * 
		 * The what_localized text is a text giving the user an idea of what happened and what to do.
		 * 
		 * The what_dev text is a text for making it easy to report bugs to the developers.
		 * 
		 * The what_code text is a search term the user can use for searching on the internet. This helps him finding soloutions to his problem. This non-localized version helps him finding more, even if it isn't in his own language.
		 * 
		 * The what_code_localized text is a localized search term for searching the internet.
		 * 
		 * The what_code_dev text is a term the developers can identify quickly as the correct error.
		 * 
		 * The difference between what_code and what_code_dev is that what_code is easily readable by humans and search engines, so things such as :: for C++ namespaces should be avoided.
		 * 
		 * The difference between what and what_dev is that what is describing the error, while what_dev is just trying to give as much information as possible in any format.
		 * 
		 * So, one can say:
		 * 
		 * The _dev texts are for letting the developers quickly identify the error and details.
		 * The _localized texts are for letting the user read the message in his preferred language.
		 * The other texts act as fallback or for finding more.
		 * 
		 * The _code_ texts are for having a single word (ideally) for quickly finding the error.
		 * The other texts are a longer description.
		 * 
		 * So, all of the texts have their reason. That should explain why you would want to have such many texts.
		 * 
		 * It is recommended to make your errors inherit from this.
		 * 
		 * This is an error interface, meaning you shouldn't throw it directly. It is there so you can catch a group of errors with a single catch statement.
		 * This interface should catch all errors, although for that you should add an extra catch(...) statement for also catching errors not inheriting from this.
		 */
		class error {
			public:
				/*!
				 * @brief What has happened.
				 * 
				 * This shouldn't be localized.
				 * 
				 * This should be a text readable by the users of your application/library. It may give a keyword to them for searching on the internet.
				 * 
				 * @return A human-readable text describing what happened.
				 */
				virtual const char* what() const noexcept;
				
				/*!
				 * @brief What has happened.
				 * 
				 * This is the localized version of @ref dsal::error::error::what()const.
				 * If localization is not supported, should return nullptr.
				 * 
				 * It is intentional that this is not noexcept: While translating things may go wrong.
				 * 
				 * @return A human-readable text describing what happened.
				 * @sa dsal::error::error::what()const
				 */
				virtual const char* what_localized() const;
				
				/*!
				 * @brief What has happened.
				 * 
				 * This should be a text to copy&paste into your bugtracker if this is a bug.
				 * 
				 * You probably want to collect as much information as possible in here, but make sure that if an error happens, you have a placeholder text.
				 * 
				 * @return Text containing all bug-tracker information.
				 */
				virtual const char* what_dev() const noexcept;
				
				/*!
				 * @brief What happened.
				 * 
				 * This shouldn't be localized.
				 * 
				 * This should be a code word for what happened. This shouldn't end in a newline.
				 * 
				 * @return A human readable error code.
				 */
				virtual const char* what_code() const noexcept;
				
				/*!
				 * @brief What happened.
				 * 
				 * This is the localized version of @ref dsal::error::error::what_code()const noexcept.
				 * 
				 * This should return nullptr if localization is not supported.
				 * 
				 * It is intentional that this is not noexcept: While translating things may go wrong.
				 * 
				 * @return A human readable localized error code.
				 * @sa dsal::error::error::what_code()const noexcept
				 */
				virtual const char* what_code_localized() const;
				
				/*!
				 * @brief What happened.
				 * 
				 * This should be a word (or more) the application/library developers can understand. This shouldn't end in a newline.
				 * 
				 * Usually, this will be the name of the class of the error.
				 * 
				 * @return Usually the name of the class.
				 */
				virtual const char* what_code_dev() const noexcept;
				
				virtual ~error();
		};
	}
}

#endif
