/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_ERROR_ERROR_DSAL_HPP
#define DSAL_ERROR_ERROR_DSAL_HPP

/*!
 * @file
 * @brief The file defining the @ref dsal::error::error_dsal error.
 */

#include "error.hpp"

namespace dsal {
	namespace error {
		/*!
		 * @brief An error in libdsal.
		 * 
		 * All errors thrown by libdsal inherit from this.
		 * 
		 * This is an error interface, meaning you shouldn't throw it directly. It is there so you can catch a group of errors with a single catch statement.
		 * This interface is for all errors thrown by libdsal.
		 */
		class error_dsal : public error {
			public:
				virtual const char* what() const noexcept override;
				virtual const char* what_localized() const override;
				virtual const char* what_dev() const noexcept override;
				virtual const char* what_code() const noexcept override;
				virtual const char* what_code_localized() const override;
				virtual const char* what_code_dev() const noexcept override;
		};
	}
}

#endif
