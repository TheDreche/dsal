/* Copyright (C) 2021 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSAL_EXPORT_HPP
#define DSAL_EXPORT_HPP

/*!
 * @file
 * @brief The file defining the libdsal macros for exporting symbols.
 * 
 * This mainly defines LIBDSAL_EXPORT based on what ./configure has found out.
 * 
 * This file is special:
 * At installation, another file gets installed at its location defining LIBDSAL_EXPORT it to empty.
 */

#include "config.hpp"

#ifdef HAVE_VISIBILITY
# define LIBDSAL_EXPORT __attribute__((visibility("default")))
#else
# define LIBDSAL_EXPORT
#endif

#endif
