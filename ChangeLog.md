# Changelog

This project follows [semantic versioning](https://semver.org/).

Releases are structured into headings, which are separated into INI-alike sections representing the part of libDSAL

## Unreleased

### C library

The following C library symbols are now also availible in the `dsal` namespace:

**[memory]**
- `size_t` as `size`
- `calloc`
- `malloc`
- `free`

They are adopted to C++ features so they can, for example, throw exceptions.

### Added

For more deatails on these, see the documentation.

**[data]**
- Added `dsal::data::abstract_class`
- Added `dsal::data::callback`

**[math]**
- Added `dsal::math::safe_int<class _integer_type>`

### Errors

The following gives an overview of the possible errors and changes:

(error interfaces are errors not to be thrown directly but for getting catched)

**[error]**
- Added general error interface `dsal::error::error`
- Added library error interface `dsal::error::error_dsal`

For making it easy to create your own exceptions, a group of utilities has
been added. See the documentation for `dsal::error::errorinfo::collect` for deatails
and an usage example.

**[data]**
- Added error interface `dsal::data::error`
- Added error `dsal::data::error_index_out_of_bounds`

**[memory]**
- Added error interface `dsal::memory::error`
- Added error `dsal::memory::error_memory_not_enough`
- `calloc` and `malloc` may throw `error_memory_not_enough`

**[math]**
- Added error interface `dsal::math::error`
The errors below currently are just thrown by the safe_int template class.
- Added error interface `dsal::math::error_type_range_too_small`
- Added error interface `dsal::math::error_overflow`
- Added error `dsal::math::error_overflow_typed`
- Added error interface `dsal::math::error_underflow`
- Added error `dsal::math::error_underflow_typed`
- Added error interface `dsal::math::error_division_by_zero`
- Added error `dsal::math::error_division_by_zero_typed`

### Convenience headers

If you are too lazy to include the required headers one by one, you can use these as a shortcut:

- Added `dsal.hpp`: includes every other header
- Added `dsal/data.hpp`: Includes data structure headers
- Added `dsal/error.hpp`: Includes errorinfo utilities and general libDSAL errors
- Added `dsal/math.hpp`: Includes math related symbol headers
- Added `dsal/memory.hpp`: Includes memory management related symbol headers
- Added `dsal/memory/allocators.hpp`: includes all allocator related headers
- Added `dsal/memory/stdalloc.hpp`: includes all c standard library allocator related headers
