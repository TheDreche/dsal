/* Copyright (C) 2021 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_NEW
# include <new>
#endif

#include "dsal/memory/error_memory_not_enough.hpp"

namespace dsal {
	namespace memory {
		const char* error_memory_not_enough::what() const noexcept {
			return "Your system ran out of free memory, but the application needs more memory.\n";
		}
		
		const char* error_memory_not_enough::what_localized() const {
			return nullptr;
		}
		
		const char* error_memory_not_enough::what_dev() const noexcept {
			return
				"error type: dsal::error_memory_not_enough\n"
				"<No details availible>\n";
		}
		
		const char* error_memory_not_enough::what_code() const noexcept {
			return "system ran out of free memory";
		}
		
		const char* error_memory_not_enough::what_code_localized() const {
			return nullptr;
		}
		
		const char* error_memory_not_enough::what_code_dev() const noexcept {
			return "dsal::error_memory_not_enough";
		}
	}
}
