/* Copyright (C) 2021 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_NEW
# include <new>
#endif

#include "dsal/math/error_overflow.hpp"

namespace dsal {
	namespace math {
		const char* error_overflow::what() const noexcept {
			return "A numer too large has been calculated.\n";
		}
		
		const char* error_overflow::what_localized() const {
			return nullptr;
		}
		
		const char* error_overflow::what_dev() const noexcept {
			return
				"error type: dsal::error_overflow\n"
				"<No details availible>\n";
		}
		
		const char* error_overflow::what_code() const noexcept {
			return "overflow";
		}
		
		const char* error_overflow::what_code_localized() const {
			return nullptr;
		}
		
		const char* error_overflow::what_code_dev() const noexcept {
			return "dsal::error_overflow";
		}
	}
}
