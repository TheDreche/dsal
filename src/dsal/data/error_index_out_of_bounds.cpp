/* Copyright (C) 2021, 2022 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_NEW
# include <new>
#endif

#include "dsal/data/error_index_out_of_bounds.hpp"
#include "dsal/error/errorinfo.hpp"
#include "dsal/memory/error_memory_not_enough.hpp"

namespace dsal {
	namespace data {
		error_index_out_of_bounds::error_index_out_of_bounds(memory::size size, memory::size index) : size(size), index(index) {}
		
		error_index_out_of_bounds::~error_index_out_of_bounds() {}
		
		const char* error_index_out_of_bounds::what() const noexcept {
			return "The application tried to access an element past the last one in a list.\n";
		}
		
		const char* error_index_out_of_bounds::what_localized() const {
			return nullptr;
		}
		
		const char* error_index_out_of_bounds::what_dev() const noexcept {
			if(what_dev_result != nullptr) {
				return what_dev_result;
			}
			const char* result = "";
			try {
				what_dev_result = dsal::error::errorinfo::collect(
					dsal::error::errorinfo::info<memory::size>("Actual size", size),
					dsal::error::errorinfo::info<memory::size>("Accessed element", index)
				);
				result = what_dev_result;
			} catch(memory::error_memory_not_enough) {
				result =
					"error type: dsal::error_index_out_of_bounds\n"
					"<While fetching data: dsal::error_memory_not_enough>\n";
			} catch(...) {
				result =
					"error type: dsal::error_index_out_of_bounds\n"
					"<An error occurred while fetching data>\n";
			}
			return result;
		}
		
		const char* error_index_out_of_bounds::what_code() const noexcept {
			return "index out of bounds";
		}
		
		const char* error_index_out_of_bounds::what_code_localized() const {
			return nullptr;
		}
		
		const char* error_index_out_of_bounds::what_code_dev() const noexcept {
			return "dsal::data::error_index_out_of_bounds";
		}
	}
}
