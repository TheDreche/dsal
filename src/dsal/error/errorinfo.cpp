/* Copyright (C) 2021 Dreche
 * 
 * This file is part of libdsal.
 * 
 * libdsal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdsal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdsal.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_CSTRING
# include <cstring>
#endif

#ifdef HAVE_CSTDIO
# include <cstdio>
#endif

#include "dsal/error/errorinfo.hpp"
#include "dsal/memory/size.hpp"

namespace dsal {
	namespace error {
		namespace errorinfo {
			memory::size info_len(const char* c) noexcept {
				return std::strlen(c);
			}
			memory::size info_len(int i) noexcept {
				return std::snprintf(nullptr, 0, "%i", i);
			}
			memory::size info_len(memory::size num) noexcept {
				return std::snprintf(nullptr, 0, "%zu", (std::size_t) num);
			}
			
			void info_str(const char*const& c, char*& to) noexcept {
				std::strcpy(to, c);
				to += std::strlen(c);
			}
			void info_str(const int& i, char*& to) noexcept {
				auto size = std::sprintf(to, "%i", i);
				if(size > 0) {
					to += size;
				} else {
					// Error, shouldn't happen
				}
			}
			void info_str(const memory::size& i, char*& to) noexcept {
				auto size = std::sprintf(to, "%zu", (std::size_t) i);
				if(size > 0) {
					to += size;
				} else {
					// Error, shouldn't happen
				}
			}
		}
	}
}
